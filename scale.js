"use strict";

var HID = require('node-hid'),
    http = require('http'),
    express = require('express'),
    rl = require('readline'),
    ip = require('./get_ip'),
    pgsql = require('./psql'),
    client = pgsql.client(),
    app = express(),
    server = http.createServer(app),
    num_devices = HID.devices().length,
    scale = "",
    scanner = "",
    scale_ready = false,
    scanner_ready = false,
    port = 8181,
    IPaddress = ip.getIP();


client.connect();


function setDevices(){
    HID.devices().forEach(function(device){
        if(device.productId == 5){
            scale = new HID.HID(device.path);
            scale_ready = true;
            checkPeripheralInDatabase(IPaddress, device.path, "scale");
        }
        if(device.productId == 1294){
            scanner = new HID.HID(device.path);
            scanner_ready = true;
            checkPeripheralInDatabase(IPaddress, device.path, "scanner");
        }
    });
}

function checkPeripheralInDatabase(address, name, ptype){
    var peripherals = client.query(
        "SELECT COUNT(*) FROM peripherals \
        WHERE name = '"+name+"' \
        AND address = '"+address+"' \
        AND ptype = '"+ptype+"';");
    peripherals.on('error', function(error){
        console.error(error);
    });
    peripherals.on('row', function(row){
        if (row.count == 0){
            storePeripheralToDatabase(address, name, ptype);
        }
    })
}

function storePeripheralToDatabase(address, name, ptype){
    var peripherals = client.query(
        "INSERT INTO peripherals (address, name, account_id, location_id, ptype)\
         VALUES ('"+address+"', '"+name+"', 1, 1, '"+ptype+"')");
    peripherals.on('error', function(error){
        console.error(error);
    });
}

setDevices();
var io = require('socket.io').listen(server);

var weight = []
var count = 0
var digits = []
var scanner_count = 0

    if(scale_ready){
        scale.on("data", function(data){
            var bad_array = [
                '\u0000S\u0000\u0000',
                '\u0000W\u0000\u0000',
            ]
            if (count < 20 && count % 2 == 0 && !contains(bad_array, data.toString('utf16le'))){
                weight.push(key[data.toString('utf16le')])
            }
            if(count == 19){
                display_weight()
            }
            if(count == 23){
                count = -1
                weight = []
            }
            count++
        });
    }
    if(scanner_ready){
        scanner.on("data", function(data){
            if (scanner_count < 32 && scanner_count % 2 == 0){
                digits.push(scanner_key[data.toString('hex', 3, 4)])
            }
            if(scanner_count == 31){
                display_barcode()
            }
            if(scanner_count == 33){
                scanner_count = -1
                digits = []
            }
            scanner_count++
        });

        scanner.on("error", function(data){
            console.log(data);
        })
    }

app.get('/get_plant/:barcode', function(req, res){
    var barcode = req.params.barcode
    res.send('ok')
})

server.listen(port)

function display_weight(){
    // console.log(weight.join(''));
    io.emit('scale_data', { reading: weight.join('')})
}

function contains(a, obj){
    for(var i = 0; i < a.length; i++){
        if(a[i] === obj){
            return true;
        }
    }
    return false;
}

var display_barcode = function(){
    io.emit('scanner_data', { reading: digits.join('')})
}

var scanner_key = {
    '1e' : '1',
    '1f' : '2',
    '20' : '3',
    '21' : '4',
    '22' : '5',
    '23' : '6',
    '24' : '7',
    '25' : '8',
    '26' : '9',
    '27' : '0',
}

var key = {
    "\u0000Y\u0000\u0000" : "1",
    "\u0000Z\u0000\u0000" : "2",
    "\u0000[\u0000\u0000" : "3",
    "\u0000\\\u0000\u0000" : "4",
    "\u0000]\u0000\u0000" : "5",
    "\u0000^\u0000\u0000" : "6",
    "\u0000_\u0000\u0000" : "7",
    "\u0000`\u0000\u0000" : "8",
    "\u0000a\u0000\u0000" : "9",
    "\u0000b\u0000\u0000" : "0",
    "\u0000c\u0000\u0000" : "."
}

var clients = {}

io.sockets.on('connection', function(socket){
    clients[socket.id] = socket;
    socket.on('disconnect', function(){
        delete clients[socket.id];
    })
})

console.log('Server started at port: '+ port);
