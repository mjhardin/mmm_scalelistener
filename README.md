# Getting Started #

This node.js will listen to data from an A&D scale and reformat the data into something human-readable.

### Installation Instructions ###

From the command line at the root of this repository issue the following command to install dependencies.
`npm install`

### Starting Server ###

To start the server issue the following command from the command line at the root of this repositry.
`sudo forever start scale.js`