"use strict";

var HID = require('node-hid');

var device = new HID.HID('Bluetooth_0044_050e_666a7c01');

var digits = []
var count = 0

device.on("data", function(data){
    if (count < 32 && count % 2 == 0){
        digits.push(key[data.toString('hex', 3, 4)])
    }
    if(count == 31){
        display_barcode()
    }
    if(count == 33){
        count = -1
        digits = []
    }
    count++
});

var display_barcode = function(){
    console.log(digits.join(''))
}

var key = {
    '1e' : '1',
    '1f' : '2',
    '20' : '3',
    '21' : '4',
    '22' : '5',
    '23' : '6',
    '24' : '7',
    '25' : '8',
    '26' : '9',
    '27' : '0',
}